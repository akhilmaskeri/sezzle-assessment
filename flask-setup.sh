echo "Setting docker context"
eval $(minikube -p minikube docker-env)

echo "building local flask-app image"
docker build ./flask_app --tag flask-app:latest

echo "creating deployment and service"
kubectl apply -f ./k8/deployment.yml -f ./k8/service.yml

minikube service my-app --url
