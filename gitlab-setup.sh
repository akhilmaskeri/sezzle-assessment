# deploying gitlab on minikube
helm repo add gitlab https://charts.gitlab.io/
helm repo update

helm upgrade --install gitlab gitlab/gitlab \
  --timeout 600s \
  -f https://gitlab.com/gitlab-org/charts/gitlab/raw/master/examples/values-minikube-minimum.yaml \
  --set global.hosts.domain=$(minikube ip).nip.io \
  --set global.hosts.externalIP=$(minikube ip) \
  --set global.edition=ce

WEBSERVICE_POD_NAME=$(kubectl get pod | grep "webservice" | cut -d " " -f 1)

sp="/-\|"
echo -n 'waiting for gitlab to comeup  '
while [[ $(kubectl get pod $WEBSERVICE_POD_NAME -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]];
do
  printf "\b${sp:i++%${#sp}:1}"
done

echo "Gitlab is ready"

#GITLAB_ROOT_USERNAME="root"
#GITLAB_ROOT_PASSWORD=$(kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode;)

# docs referred: 
# https://docs.gitlab.com/ee/api/users.html#user-creation
