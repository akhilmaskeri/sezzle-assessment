#!/usr/bin/env bash

echo "Starting minikube"
minikube start

# deployment 1:
# GitLab Deployment with a default account
# status: incomplete
#./gitlab-setup.sh

# deployment 2: 
# Jenkins Deployment with a default account
# username: admin@sezzle.in / password: jenkins@123!
# status: complete
./jenkins-setup.sh

# deployment 3:
# Flask Gunicorn Server
# status: complete
./flask-setup.sh
