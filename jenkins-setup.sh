# deploying jenkins on minikube
helm repo add cloudposse https://charts.cloudposse.com/incubator/
helm repo update
helm install jenkins jenkins/jenkins --version=3.0.14


# Wait till the jenkins comes up
i=1
sp="/-\|"

echo -n 'waiting for jenkins to comeup  '

while [[ $(kubectl get pod jenkins-0 -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]];
do
  printf "\b${sp:i++%${#sp}:1}"
done

echo "Jenkins is ready."

# Using port-forwarding
kubectl --namespace default port-forward svc/jenkins 8080:8080 &

# Creating a user account
wget http://localhost:8080/jnlpJars/jenkins-cli.jar -P /tmp/

JENKINS_ADMIN_PASSWORD=$(kubectl exec --namespace default -it svc/jenkins -c jenkins -- /bin/cat /run/secrets/chart-admin-password)

# Creating the default account
echo 'jenkins.model.Jenkins.instance.securityRealm.createAccount("admin@sezzle.in", "jenkins@123!")' | \
      java -jar /tmp/jenkins-cli.jar -s "http://localhost:8080" -auth admin:$JENKINS_ADMIN_PASSWORD groovy = 

echo "created admin@sezzle.in account"

echo "cleaning up"
rm /tmp/jenkins-cli.jar
